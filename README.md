# 统一资源存储sdk

## 获取

- Coding：https://cloudladder.coding.net/p/business-common/d/middle-office-storage/git
- Packagist：https://packagist.org/packages/gupo/middle-office-storage
- Composer：`composer require gupo/middle-office-storage`

## 环境

- PHP7.2+ | 8.0+
- Laravel6.2+ /Laravel9+

## 支持

- 本地存储
- 阿里系 OSS
- AWS S3协议 OSS

## 安装

### 执行安装

命令：`php artisan gupo:middle-office-storage:install`

### 防重复安装

如果app目录下，已存在`Service/Storage/StorageService.php`文件，则不会再执行安装程序

### 安装做了些什么？

1. 发布资源：
    1. 4个存储磁盘实例
        1. `Service/Storage/Modules/AwsS3.php`
        2. `Service/Storage/Modules/Oss.php`
        3. `Service/Storage/Modules/Local.php`
        4. `Service/Storage/Modules/LocalPublic.php`
    2. 访问出口类：`Service/Storage/StorageService.php`

## 文件结构

```
/
└── /app
    └── /Services # 服务层
        └── /Storage # Storage服务
            ├── /Modules # 模块层
            │   └──AwsS3.php # 子类-存储磁盘实例
            │   └──Oss.php # 子类-存储磁盘实例
            │   └──Local.php # 子类-存储磁盘实例
            │   └──LocalPublic.php # 子类-存储磁盘实例
            │   ...
            └── StorageService.php # 存储磁盘访问出口
```

## 配置-config/filesystems.php

```php
    'disks'      => [

        'local' => [
            'driver' => 'local', // 本地
            'root'   => storage_path('app'),
        ],

        'public' => [
            'driver'     => 'local', // 本地
            'root'       => storage_path('app/public'),
            'url'        => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'oss' => [
            'driver'         => 'oss', // 阿里系
            'root'           => '',
            'access_key'     => env('OSS_ACCESS_KEY_ID'),
            'secret_key'     => env('OSS_ACCESS_KEY_SECRET'),
            'url'            => env('OSS_URL'),
            'endpoint'       => env('OSS_REGION'),
            'bucket'         => env('OSS_BUCKET'),
            'isCName'        => false,
            // 额外的配置
            'visibility'     => env('OSS_VISIBILITY', 'private'), // 可见性
            'rewrite_prefix' => env('OSS_REWRITE_PREFIX'), // 运维重写修正路径前缀
            'rewrite_domain' => env('OSS_REWRITE_DOMAIN'), // 重写域名
        ],

        's3' => [
            'driver'                  => 's3', // S3协议
            'key'                     => env('OSS_AWS_ACCESS_KEY_ID'),
            'secret'                  => env('OSS_AWS_SECRET_ACCESS_KEY'),
            'region'                  => env('OSS_AWS_DEFAULT_REGION'),
            'bucket'                  => env('OSS_AWS_BUCKET'),
            'url'                     => env('OSS_AWS_URL'),
            'endpoint'                => env('OSS_AWS_ENDPOINT'),
            'use_path_style_endpoint' => true,
            // 额外的配置
            'visibility'              => env('OSS_AWS_VISIBILITY', 'private'), // 可见性
            'rewrite_prefix'          => env('OSS_AWS_REWRITE_PREFIX'), // 重写路径前缀
            'rewrite_domain'          => env('OSS_AWS_REWRITE_DOMAIN'), // 重写域名
        ],

    ],

    // 上传修正项目
    'fix_option' => [
        'app_path'    => env('FILESYSTEM_APP_PATH', ''), // 应用路径
        'is_relative' => env('FILESYSTEM_IS_RELATIVE', true), // 是否最终返回相对地址
    ],

    // OSS默认磁盘
    'oss_disk'   => env('OSS_DISK', 'oss'), // OSS默认磁盘，对应"disks"的键名
```

## 配置-.env
```dotenv
# OSS默认磁盘，对应"disks"的键名
OSS_DISK=

# OSS配置-阿里系
OSS_ACCESS_KEY_ID=
OSS_ACCESS_KEY_SECRET=
OSS_URL=
OSS_REGION=
OSS_BUCKET=

OSS_VISIBILITY=
OSS_REWRITE_PREFIX=
OSS_REWRITE_DOMAIN=

# OSS配置-AWS S3协议
OSS_AWS_ACCESS_KEY_ID=
OSS_AWS_SECRET_ACCESS_KEY=
OSS_AWS_DEFAULT_REGION=
OSS_AWS_BUCKET=
OSS_AWS_URL=
OSS_AWS_ENDPOINT=

OSS_AWS_VISIBILITY=
OSS_AWS_REWRITE_PREFIX=
OSS_AWS_REWRITE_DOMAIN=

# OSS文件系统 全局配置
FILESYSTEM_APP_PATH=
FILESYSTEM_IS_RELATIVE=
```


## 继承

- "本地存储实例"继承`Gupo\MiddleOfficeStorage\StorageLocalClient`
- "OSS存储实例"继承`Gupo\MiddleOfficeStorage\StorageOssClient`

## 如何区分本地存储与OSS存储？

请看在"config/filesystems.php"文件中，"disks"下的配置。

如果"driver"为"local"则为"本地存储"，否则认为是"OSS存储"。

## "存储磁盘实例"类初始化

### 初始化-本地存储实例

```php
 /**
  * 获取-磁盘标识
  *
  * @return string
  */
 public function getDiskCode(): string
 {
     return 'local'; // "config/filesystems.php"文件中，"disks"下的键名
 }
```

### 初始化-OSS存储实例

```php
 /**
  * 获取-磁盘标识
  *
  * @return string
  */
 public function getDiskCode(): string
 {
     return 's3'; // "config/filesystems.php"文件中，"disks"下的键名
 }

 /**
  * 获取-本地临时存储实例
  *
  * @return StorageLocalClient
  */
 public function getLocalTmpStorage(): StorageLocalClient
 {
     return StorageService::local(); // todo 此处返回一个本地存储实例
 }
```

## 使用

### 使用-OSS存储实例

```php
/**
 * 文件上传 - 二进制
 *
 * @param File|UploadedFile $file
 * @param string            $dir_path    文件夹路径
 * @param bool              $is_date_dir 是否区分日期目录
 * @return string
 * @author lyl
 */
public function fileUploadByBinary($file, string $dir_path, bool $is_date_dir = true): string;

/**
 * 文件上传 - Base64
 *
 * @param string $base64FileString Base64文件字符串，例：以'data:image/png;base64,'格式开头的字符串
 * @param string $dir_path         文件夹路径
 * @param bool   $is_date_dir      是否区分日期目录
 * @return string
 * @throws Exception
 * @author lyl
 */
public function fileUploadByBase64(string $base64FileString, string $dir_path, bool $is_date_dir = true): string;

/**
 * 文件上传 - 文件内容
 *
 * @param string $file_contents 文件内容
 * @param string $dir_path      文件夹路径
 * @param string $file_postfix  文件后缀
 * @param bool   $is_date_dir   是否区分日期目录
 * @return string
 * @throws StorageException
 * @throws Exception
 * @author lyl
 */
public function fileUploadByContents(string $file_contents, string $dir_path, string $file_postfix, bool $is_date_dir = true): string;

/**
 * 获取-访问Url
 *
 * @param string    $key
 * @param bool|null $is_relative  是否返回相对路径：true-相对路径；false-全链接；(如果调用时给值，则取该值；如果调用时没给值，则取配置上的值；如果都没有值，则默认为false)
 * @param int|null  $sign_timeout 签名有效时间/秒（如果调用时给值，则取该值，如果调用时没给值，则取统一配置）
 * @return string
 * @throws Exception
 * @author lyl
 */
public function getVisitUrl(string $key, ?bool $is_relative = null, ?int $sign_timeout = null): string;

```

### 使用-本地存储实例

```php
/**
 * 写文件到本地 - Base64
 *
 * @param string $base64FileString Base64文件字符串
 * @param string $dir_path         文件夹路径
 * @return string 文件相对路径
 * @throws StorageException
 * @throws Exception
 * @author lyl
 */
public function putFileToLocalByBase64(string $base64FileString, string $dir_path): string;

/**
 * 写文件到本地 - 文件内容
 *
 * @param string $file_contents
 * @param string $dir_path
 * @param string $file_name
 * @return string 文件相对路径
 * @throws StorageException
 * @author lyl
 */
public function putFileToLocalByContents(string $file_contents, string $dir_path, string $file_name): string;

/**
 * 获取本地文件的完整路径
 *
 * @param string $file_path
 * @return string
 * @author lyl
 */
public function getLocalFileFullPath(string $file_path): string;

/**
 * 删除文件
 *
 * @param string $path 文件路径
 * @return bool
 * @throws Exception
 * @author lyl
 */
public function delete(string $path): bool;
```

## 访问

访问实例-示例：`StorageService::ossService()`

上传-示例：

```php
$file = $request->file('file');

// 文件上传
$filePath = StorageService::ossService()->fileUploadByBinary($file, $this->testFolderPath);
// 访问链接
$visitUrl = StorageService::ossService()->getVisitUrl($filePath);

return [
   'file_path' => $filePath,
   'visit_url' => $visitUrl,
];
```

## 测试示例

在`src/Test`目录中，有调用示例，可作参考。
