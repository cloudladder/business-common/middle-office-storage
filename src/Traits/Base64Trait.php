<?php
declare(strict_types = 1);
/**
 * Base64
 */

namespace Gupo\MiddleOfficeStorage\Traits;

use Gupo\MiddleOfficeStorage\Exceptions\StorageException;

trait Base64Trait
{

    /**
     * 解析【Base64编码文件】
     *
     * @param string $base64_file_string
     * @return array
     * @throws \Exception
     * @author lyl
     */
    public function parseBase64File(string $base64_file_string): array
    {
        // 正则
        $pattern = '/^data:\s*\w+\/(\w+);base64,/';

        // 匹配次数
        $match_num = preg_match($pattern, $base64_file_string, $matches);
        if (empty($match_num)) {
            throw new StorageException('Base64内容解析异常，内容不合法！');
        }

        // Base头
        $base_head = $matches[0] ?? null;
        // 文件类型
        $file_type = $matches[1] ?? null;

        // base64内容
        $base64_contents = str_replace($base_head, '', $base64_file_string);

        // 解析文件内容
        $file_contents = base64_decode($base64_contents);
        if (false === $file_contents) {
            throw new StorageException('Base64内容解析失败！');
        }

        return [
            'base_head'       => $base_head, // Base头
            'file_type'       => $file_type, // 文件类型（后缀）
            'base64_contents' => $base64_contents, // base64内容
            'file_contents'   => $file_contents, // 解析后的文件内容
        ];
    }

}