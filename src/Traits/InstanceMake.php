<?php
declare(strict_types = 1);

namespace Gupo\MiddleOfficeStorage\Traits;

use Illuminate\Support\Facades\App;

trait InstanceMake
{
    /**
     * Make Class
     *
     * 非单例/支持构建函数传参/不支持构造函数依赖注入
     *
     * @param array $parameters
     *
     * @return static
     */
    public static function make(...$parameters)
    {
        return new static(...$parameters);
    }

    /**
     * Resolve Class
     *
     * 非单例/不支持构建函数传参/支持构造函数依赖注入
     *
     * @return static
     */
    public static function resolve()
    {
        return App::make(static::class);
    }

    /**
     * Instance Class
     *
     * 单例/不支持构建函数传参/支持构造函数依赖注入
     *
     * @param bool $force 强制重新初始化
     *
     * @return static
     */
    public static function instance(bool $force = false)
    {
        if ($force) {
            App::singleton(static::class);
        } else {
            App::singletonIf(static::class);
        }

        return static::resolve();
    }
}