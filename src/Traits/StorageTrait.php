<?php
declare(strict_types = 1);

namespace Gupo\MiddleOfficeStorage\Traits;

trait StorageTrait
{

    /**
     * 是否是本地存储实例
     *
     * @return bool true-本地存储；false-非本地存储
     * @author lyl
     */
    public function isLocal(): bool
    {
        if ($this->disk_config['driver'] == 'local') {
            return true;
        } else {
            return false;
        }
    }

}