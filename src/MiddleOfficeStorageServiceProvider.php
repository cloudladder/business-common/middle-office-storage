<?php

declare(strict_types = 1);
/**
 * 服务提供者，在composer中暴露
 */

namespace Gupo\MiddleOfficeStorage;

use Gupo\MiddleOfficeStorage\Console\InstallCommand;
use Illuminate\Support\ServiceProvider;

class MiddleOfficeStorageServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    /**
     * 启动所有的应用服务
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCommands(); // 注册-自定义命令行脚本

        $this->registerPublishing(); // 注册-可发布资源

    }

    /**
     * 注册-自定义命令行脚本
     *
     * @author lyl
     */
    public function registerCommands(): void
    {
        if (! $this->app->runningInConsole()) { // 非-控制台中运行
            return;
        }

        $this->commands([
            InstallCommand::class,
        ]);
    }

    /**
     * 注册-可发布资源
     *
     * @author lyl
     */
    public function registerPublishing(): void
    {
        if (! $this->app->runningInConsole()) { // 非-控制台中运行
            return;
        }

        // 发布Api服务
        $this->publishes([
            __DIR__ . '/../stubs/Services/Storage/Modules/AwsS3.stub'       => app_path('Services/Storage/Modules/AwsS3.php'),
            __DIR__ . '/../stubs/Services/Storage/Modules/Oss.stub'         => app_path('Services/Storage/Modules/Oss.php'),
            __DIR__ . '/../stubs/Services/Storage/Modules/Local.stub'       => app_path('Services/Storage/Modules/Local.php'),
            __DIR__ . '/../stubs/Services/Storage/Modules/LocalPublic.stub' => app_path('Services/Storage/Modules/LocalPublic.php'),
            __DIR__ . '/../stubs/Services/Storage/StorageService.stub'      => app_path('Services/Storage/StorageService.php'),
        ], 'gupo:middle-office-storage:storage-client');

    }


}
