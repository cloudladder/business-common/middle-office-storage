<?php
declare(strict_types = 1);


namespace Gupo\MiddleOfficeStorage\Exceptions;

class StorageException extends \Exception implements \Throwable
{

}