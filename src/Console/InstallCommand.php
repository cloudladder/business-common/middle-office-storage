<?php
declare(strict_types = 1);
/**
 * MiddleOfficeStorage 安装
 */

namespace Gupo\MiddleOfficeStorage\Console;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    protected $signature = 'gupo:middle-office-storage:install';

    protected $description = '安装：统一资源存储客户端';

    /**
     * 执行
     *
     * @author lyl
     */
    public function handle()
    {
        // 发布-资源文件
        $this->publishesResources();

    }

    /**
     * 发布-资源文件
     *
     * @author lyl
     */
    protected function publishesResources()
    {
        if (file_exists(app_path('Services/Storage/StorageService.php'))) { // 防止重复安装
            return;
        }
        $this->comment('发布"MiddleOfficeStorage"基础文件');
        $this->callSilent('vendor:publish', [ '--tag' => 'gupo:middle-office-storage:storage-client' ]);
    }


}