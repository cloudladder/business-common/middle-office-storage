<?php
declare(strict_types = 1);
/**
 * 存储客户端
 */

namespace Gupo\MiddleOfficeStorage\Foundation;

use Gupo\MiddleOfficeStorage\Traits\Base64Trait;
use Gupo\MiddleOfficeStorage\Traits\InstanceMake;
use Gupo\MiddleOfficeStorage\Traits\StorageTrait;

abstract class StorageClient
{
    use InstanceMake, StorageTrait, Base64Trait;
}