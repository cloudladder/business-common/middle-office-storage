<?php
declare(strict_types = 1);
/**
 * 本地存储-客户端
 */

namespace Gupo\MiddleOfficeStorage;

use Exception;
use Gupo\MiddleOfficeStorage\Exceptions\StorageException;
use Gupo\MiddleOfficeStorage\Foundation\StorageClient;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

abstract class StorageLocalClient extends StorageClient
{

    /**
     * @var FilesystemAdapter 磁盘实例
     */
    protected $disk;

    /**
     * @var array 磁盘-配置
     */
    protected $disk_config;


    /**
     * @throws StorageException
     */
    public function __construct()
    {
        $disk_code  = $this->getDiskCode();
        $this->disk = Storage::disk($disk_code);

        $this->disk_config = config('filesystems.disks.' . $disk_code);

        if (! $this->isLocal()) {
            throw new StorageException('系统异常，当前存储实例为非本地存储！' . __CLASS__ . ' ' . $disk_code);
        }

        // 如果需要进行额外的"个性化的初始化"，则封装到“init”方法中
        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * 获取-磁盘标识
     *
     * @return string
     * @author lyl
     */
    abstract public function getDiskCode(): string;

    /**
     * 写文件到本地 - Base64
     *
     * @param string $base64FileString Base64文件字符串
     * @param string $dir_path         文件夹路径
     * @return string 文件相对路径
     * @throws StorageException
     * @throws Exception
     * @author lyl
     */
    public function putFileToLocalByBase64(string $base64FileString, string $dir_path): string
    {
        // 解析【Base64编码文件】
        $parseBase64FileRet = $this->parseBase64File($base64FileString);
        // 文件类型（后缀）
        $file_type = $parseBase64FileRet['file_type'];
        // 解析后的文件内容
        $file_contents = $parseBase64FileRet['file_contents'];

        // 临时文件名
        $file_name = date('Ymdhis') . rand(100000, 999999) . '.' . $file_type;

        return $this->putFileToLocalByContents($file_contents, $dir_path, $file_name);
    }

    /**
     * 写文件到本地 - 文件内容
     *
     * @param string $file_contents
     * @param string $dir_path
     * @param string $file_name
     * @return string 文件相对路径
     * @throws StorageException
     * @author lyl
     */
    public function putFileToLocalByContents(string $file_contents, string $dir_path, string $file_name): string
    {
        // 文件相对路径
        $file_path = trim($dir_path, '/') . '/' . trim($file_name, '/');

        // 写入文件
        $ret = $this->disk->put($file_path, $file_contents);
        if (! $ret) {
            throw new StorageException('向本地写入文件内容失败，写入异常！');
        }

        return $file_path;
    }

    /**
     * 获取本地文件的完整路径
     *
     * @param string $file_path
     * @return string
     * @author lyl
     */
    public function getLocalFileFullPath(string $file_path): string
    {
        return $this->disk_config['root'] . '/' . $file_path;
    }

    /**
     * 删除文件
     *
     * @param string $path 文件路径
     * @return bool
     * @throws Exception
     * @author lyl
     */
    public function delete(string $path): bool
    {
        return $this->disk->delete($path);
    }

}