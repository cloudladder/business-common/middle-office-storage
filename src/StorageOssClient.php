<?php
declare(strict_types = 1);
/**
 * OSS存储-客户端
 */

namespace Gupo\MiddleOfficeStorage;

use Exception;
use Gupo\MiddleOfficeStorage\Exceptions\StorageException;
use Gupo\MiddleOfficeStorage\Foundation\StorageClient;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

abstract class StorageOssClient extends StorageClient
{

    /**
     * @var FilesystemAdapter 磁盘实例
     */
    protected $disk;

    /**
     * @var StorageLocalClient 本地临时存储实例
     */
    private $local_tmp_storage;

    /**
     * @var array 磁盘-配置
     */
    protected $disk_config;

    /**
     * @var array 公共修正-配置
     */
    protected $fix_option_config;

    /**
     * @var string 可见性：public-公有（默认）；private-私有
     */
    protected $visibility;

    /**
     * @var string 应用路径
     */
    protected $app_path;

    /**
     * @var bool 应用路径
     */
    protected $is_relative;

    /**
     * @var string 重写路径前缀（一般是Bucket。如果运维做了反向代理，或S3自动拼接了bucket，则根据情况无需设置该项）
     */
    protected $rewrite_prefix;

    /**
     * @var string 重写域名（如果访问设置了代理地址，则需要设置该项；如果返回的是相对路径，则无需设置该项）
     */
    protected $rewrite_domain;

    /**
     * @var int 签名有效时长(秒)
     */
    protected $sign_timeout = 3600;


    /**
     * @throws StorageException
     */
    public function __construct()
    {
        $disk_code               = $this->getDiskCode();
        $this->disk              = Storage::disk($disk_code);
        $this->local_tmp_storage = $this->getLocalTmpStorage();

        $this->disk_config       = config('filesystems.disks.' . $disk_code);
        $this->fix_option_config = config('filesystems.fix_option');
        $this->visibility        = strval($this->disk_config['visibility'] ?? false);
        $this->rewrite_prefix    = strval($this->disk_config['rewrite_prefix'] ?? '');
        $this->rewrite_domain    = strval($this->disk_config['rewrite_domain'] ?? '');
        $this->app_path          = strval($this->fix_option_config['app_path'] ?? '');
        $this->is_relative       = boolval($this->fix_option_config['is_relative'] ?? false);

        if ($this->isLocal()) {
            throw new StorageException('系统异常，当前存储实例为本地存储！' . __CLASS__ . ' ' . $disk_code);
        }

        // 如果需要进行额外的"个性化的初始化"，则封装到“init”方法中
        if (method_exists($this, 'init')) {
            $this->init();
        }
    }

    /**
     * 获取-磁盘标识
     *
     * @return string
     * @author lyl
     */
    abstract public function getDiskCode(): string;

    /**
     * 获取-本地临时存储实例（注意：1.请在全局设置 2.OSS才会访问此方法）
     *
     * @return StorageLocalClient
     * @author lyl
     */
    abstract public function getLocalTmpStorage(): StorageLocalClient;

    // ##############################################################################

    /**
     * 文件上传 - 二进制
     *
     * @param File|UploadedFile $file
     * @param string            $dir_path    文件夹路径
     * @param bool              $is_date_dir 是否区分日期目录
     * @param string|null       $name        指定文件名
     * @param array             $options     其他设置项
     * @return string
     * @author lyl
     */
    public function fileUploadByBinary($file, string $dir_path, bool $is_date_dir = true, ?string $name = null, array $options = []): string
    {
        // oss完整目录路径
        $oss_full_dir_path = $this->getOssFullDirPath($dir_path, $is_date_dir);

        // 上传文件到oss
        if (! $name) {
            $file_path = $this->disk->put($oss_full_dir_path, $file, $options);
        } else {
            $file_path = $this->disk->putFileAs($oss_full_dir_path, $file, $name, $options);
        }

        if ($this->visibility && 'private' == $this->visibility) {
            // 强制设为私有
            $this->disk->setVisibility($file_path, 'private');
        }

        // 返回OSS Object Key
        // 数据库建议存储该返回地址
        return '/' . $file_path;
    }

    /**
     * 文件上传 - Base64
     *
     * @param string      $base64FileString Base64文件字符串，例：以'data:image/png;base64,'格式开头的字符串
     * @param string      $dir_path         文件夹路径
     * @param bool        $is_date_dir      是否区分日期目录
     * @param string|null $name             指定文件名
     * @param array       $options          其他设置项
     * @return string
     * @throws Exception
     * @author lyl
     */
    public function fileUploadByBase64(string $base64FileString, string $dir_path, bool $is_date_dir = true, ?string $name = null, array $options = []): string
    {
        $local_tmp_storage = $this->local_tmp_storage;

        // Base64上传到本地，返回【本地临时文件】路径
        $tmp_path = $local_tmp_storage->putFileToLocalByBase64($base64FileString, 'tmp');
        // 获取【本地临时文件】完整路径
        $tmp_full_path = $local_tmp_storage->getLocalFileFullPath($tmp_path);
        // 将【本地临时文件】上传到OSS，返回OSS文件路径
        $file_path = $this->fileUploadByBinary(new File($tmp_full_path), $dir_path, $is_date_dir, $name, $options);
        // 删除【本地临时文件】
        $local_tmp_storage->delete($tmp_path);

        return $file_path;
    }

    /**
     * 文件上传 - 文件内容
     *
     * @param string      $file_contents 文件内容
     * @param string      $dir_path      文件夹路径
     * @param string      $file_postfix  文件后缀
     * @param bool        $is_date_dir   是否区分日期目录
     * @param string|null $name          指定文件名
     * @param array       $options       其他设置项
     * @return string
     * @throws StorageException
     * @throws Exception
     * @author lyl
     */
    public function fileUploadByContents(string $file_contents, string $dir_path, string $file_postfix, bool $is_date_dir = true, ?string $name = null, array $options = []): string
    {
        $local_tmp_storage = $this->local_tmp_storage;

        // 临时文件名
        $tmpFileName = date('Ymdhis') . rand(100000, 999999) . '.' . trim($file_postfix, '.');

        // 文件内容上传到本地，返回【本地临时文件】路径
        $tmp_path = $local_tmp_storage->putFileToLocalByContents($file_contents, 'tmp', $tmpFileName);
        // 获取【本地临时文件】完整路径
        $tmp_full_path = $local_tmp_storage->getLocalFileFullPath($tmp_path);
        // 将【本地临时文件】上传到OSS，返回OSS文件路径
        $file_path = $this->fileUploadByBinary(new File($tmp_full_path), $dir_path, $is_date_dir, $name, $options);
        // 删除【本地临时文件】
        $local_tmp_storage->delete($tmp_path);

        return $file_path;
    }

    /**
     * 删除文件
     *
     * @param string $path 文件路径
     * @return bool
     * @throws Exception
     * @author lyl
     */
    public function delete(string $path): bool
    {
        return $this->disk->delete($path);
    }

    /**
     * 获取-访问Url
     *
     * @param string    $key
     * @param bool|null $is_relative  是否返回相对路径：true-相对路径；false-全链接；(如果调用时给值，则取该值；如果调用时没给值，则取配置上的值；如果都没有值，则默认为false)
     * @param int|null  $sign_timeout 签名有效时间/秒（如果调用时给值，则取该值，如果调用时没给值，则取统一配置）
     * @return string
     * @throws Exception
     * @author lyl
     */
    public function getVisitUrl(string $key, ?bool $is_relative = null, ?int $sign_timeout = null): string
    {
        if (substr($key, 0, 4) == 'http') {
            return $key;
        }
        $is_relative = $is_relative ?? $this->is_relative;

        $sign_url = $this->getOssSignUrl($key, $sign_timeout);

        if ($is_relative) { // 相对地址
            $visitUrl = $this->getUrlRelativePath($sign_url);

            // 如果是相对地址，才会判断是否要修正
            if ($this->rewrite_prefix) {
                $visitUrl = '/' . $this->rewrite_prefix . $visitUrl;
            }
        } else { // 全链接
            $visitUrl = $sign_url;
            // 修正访问域名
            if ($this->rewrite_domain) {
                $visitUrl = $this->rewriteVisitDomain($visitUrl, $this->rewrite_domain);
            }
        }

        return $visitUrl;
    }

    /**
     * 获取-oss完整目录路径
     *
     * @param string $dir_path
     * @param bool   $is_date_dir 是否区分日期目录
     * @return string
     * @author lyl
     */
    protected function getOssFullDirPath(string $dir_path, bool $is_date_dir = true): string
    {
        $dirArr = [
            trim($this->app_path, '/'), // 补充上应用名称路径
            trim($dir_path, '/'),
        ];
        if ($is_date_dir) {
            $dirArr[] = date('Ymd');
        }

        $dirArr = array_filter($dirArr, function ($value) {
            if ('' === $value) {
                return false;
            } else {
                return true;
            }
        });

        return implode('/', $dirArr);
    }

    /**
     * 修正访问域名
     *
     * @param string $url
     * @param string $domain
     * @return string
     * @author lyl
     */
    protected function rewriteVisitDomain(string $url, string $domain): string
    {
        $parsed_url = parse_url($url);

        $visitUrl = trim($domain, '/');

        if (isset($parsed_url['path'])) {
            $visitUrl .= $parsed_url['path'];
        }

        if (isset($parsed_url['query'])) {
            $visitUrl .= '?' . $parsed_url['query'];
        }

        return $visitUrl;
    }

    /**
     * 辅助函数-获取URL相对路径
     *
     * @param $url
     * @return string
     * @author lyl
     */
    protected function getUrlRelativePath($url): string
    {
        $relative_path = '';

        $parsed_url = parse_url($url);

        if (isset($parsed_url['path'])) {
            $relative_path = $parsed_url['path'];
        }

        if (isset($parsed_url['query'])) {
            $relative_path .= '?' . $parsed_url['query'];
        }

        return $relative_path;
    }

    /**
     * 获取-OSS签名URL
     *
     * @param string   $file_path 支持相对或者完整地址
     * @param int|null $sign_timeout
     * @return false|string
     * @throws Exception
     * @author lyl
     */
    protected function getOssSignUrl(string $file_path, ?int $sign_timeout = null)
    {
        $parsed_url = parse_url($file_path);

        if (! isset($parsed_url['path'])) {
            return false;
        }

        $sign_timeout = $sign_timeout ?? $this->sign_timeout;

        // 签名，设置有效时间
        if ($this->disk_config['driver'] == 's3') {
            // s3 调用
            $path    = ltrim($parsed_url['path'], '/');
            $signUrl = $this->disk->temporaryUrl($path, now()->addSeconds($sign_timeout));
        } else {
            // 阿里系调用
            $signUrl = $this->disk->signUrl($parsed_url['path'], $sign_timeout);
        }

        return $signUrl;
    }

}