<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: lyl
 * Date: 2023/9/13
 * Time: 7:54 下午
 */

namespace Gupo\MiddleOfficeStorage\Test;

use App\Services\Storage\StorageService;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UploadFile
{

    public $testFolderPath = 'test/test-oss';

    public $testBase64FileString = <<<testBase64FileString
data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCACKAIoDASIAAhEBAxEB/8QAHQAAAQQDAQEAAAAAAAAAAAAAAAUHCAkCBAYBA//EAEYQAAEDAwIEAwQHAwYPAAAAAAECAwQABREGBwgSITETQVEUYXGBCRUiMkKRoSNicjNSgrHB0RYXNDdDU2NzdIOSorLC4f/EABwBAAIDAQEBAQAAAAAAAAAAAAAGAwQFBwIIAf/EADMRAAEDAgQDBgUDBQAAAAAAAAEAAgMEEQUSITEGQWETFFFxgaEiMpGx0cHh8CNCUnLx/9oADAMBAAIRAxEAPwC1OiiihCKKK1Zc9EfKU/bc9PIfGhC2FrShJUohIHma03bqhOQhJWfU9BSc8+t9WVqz7vIV86ELZcuL7n4uQeiRXxU6tf3lqV8TWFFCEV6lak9lEfA15RQhfduc+32cJ/i61tNXbycR800nUUIS+0+h9OUKB93mK+lc6lRQoKSSCPMUpRbmFYS90Pkr++hCUKK8ByK9oQiiiihCKKK1J8v2dvlSf2iu3u99CF858/wyW2z9rzV6Ul96KKEIooooQivFKCBlRCR6k1Cvfr6SS0aD1FJsOhLQxql+IstyLrJfKYnOO6Wgnq4AehVlI6dOYdaglvNv1rDfXUr911Nc3HGSomPbGFqTEip8ktt5x8VHKj5k1pQ0MkmrtAoHTNbtqruolwiz+f2aSzI5PveE4FcvxxWxVAVvuUu0yUyYMp+HIT2djuFtY+BBzTs6D4vN3duyhFs1tcZUVJ/yW6qE1vHoPFCikfwkVO7DXD5XLwJxzCukoqvnan6UZwLaibi6YSpJODc7B0IHqphxXX3lK/gmpsba7taR3fsf1tpG+RrzETgOholLrKj2S42rCkH3KAz5VnS08kPzhTte12y66iiiq69rchTiwQhfVv8AqpWBBAIOQa52lC2y+U+Cs9D90/2UISnRRRQhYrWG0FSugAyaQn3i+6pZ8/L0pQur3K0lsd1HJ+FJdCEUUUUIRUXvpA98X9p9nxZbTKVG1Bqda4bTjZwtmMkAvrB8iQpKAe/7QkdRUoaqI4+9y17hcRd6iNPeJbtOoTaI6QegWjq8ceviqWn4IHpV+ii7WUX2Gqhldlao40UUU0qggAkgAZJrqND7Z6k3GlXWLp62uXCTbIa5shlJAWG0kAhIP3lZUMJHU9cCpIfR5bf2+/6v1HqafFbku2VphqEXUhQbddKyXE/vJS3gHy5zU8WrLb2Lo7cm4MZu4uthpyYllIeWgHISV4yRnyzSTivEfcJ3U0cd3ADUnS5128uqZqDBu9xNme+wPLp/1UrEYNdPtzuXqXabVEbUGlrq9arkz0KmzlDqM9UOIPRaTjqkjHbzAqSfGBwp3Gy32frfR9vXMssxRfuECKjmXDdPVbiUju2e5x905/DjERaZqKshxGASxm4O48D4FYlTTSUcpjkH79QrneFjiQt3EfoE3JLKIGobepLF1t6FZDbhGUuIz18NeDjPUFKh1xkvRVSv0dutpOluJO12xDihDv8AFkQX0dwSltTyDj1CmgM/vH1NW1ViVcIhlyt2OqljdmbcooBwcjoaKKpKVLkOR7QwFH7w6H4196SLY94cjkPZYx86V6EJFuLniSleiela1ZvK53Vq9VE1hQhFFFFCEj6y1NG0VpG96gmkCJaoT013JxlLaCsj9KodvF1k327TblMc8WZMfXIecP4lrUVKPzJNWhfSP7wsaK2dTo+K+BedTuBtTaT9puI2oKcUfTmUEIHqFL9Kqyphw6PKwvPNUp3XNkUVMrZnh90nqPhJv+qFW1u5armQJ7jUh4FSoy2SsNobHYE+GDzYz9vGcdK1+Bzh4sOt4EvXGpYibk1EmezW+C8MslaEpUp1afx9VAAHp0VkHpirLjdNFHNIQf6bsvmeivswuaR8TAR8Yv5Dqur4atqbdt9s3D1Hq7Vt10NJv8xK2PAufsSFIIwwFIUClSlALWMj7qhnt0lFpLRT2lZMp1zU19vqH0pSGbu+06hojzRytpOT55JHuFcNxIbCL3605aLczdxZ3oEvxwtbPioWgp5VDAI6gdR18seeQ61tgotduiw21KW3HaQylSzlRCQACT69K5fX1ve4xN2l3vJzNt8oFstjZPdJTd3d2WX4WgWN9zz0TQbg23bC0eyXvcOY74l4lFhpqfPluR1Kz0QGErLaUgAZPKEjuT1qF3GxpG06M3q9hstti2qAu2R3URobKWmwftJJ5UgDJ5ck+ZqaW8XD5a994tthXG5yLYqyXB5wKjoCvEad5VrRgnoSOXCuuMHoaiBx8utnfZthoAJjWiM1yjy6uKA/JQpl4dla6eJrZXOdldmadhYjLb0WNjcZbHISwAXbYjc3BvdKH0c2jndScSlvuQQTHsMGTOcV5ZUgsJHxy9nH7p9KtmqHP0Ze2Q01tDc9YSGgJmpJhSysjr7MwVIT8MuF7PrhNTGpjrpM8xty0SrELNRRRRVBTL1CihaVDuDmuhSQpII7HrXO0pszghpCc9kgUISZRWTieVah6EisaEIpvN799NMbCaOev2o5Q51Aph25pQ9omOfzG0ny7ZV2SO/kC2fFbxk2Ph8hOWa2pbvWuXmgpmASfBiBQ+y5II7dOoQPtK6fdBCqqs3A3F1FulqaTf8AVF1fu10kHq68eiE+SEJHRCR5JSABWnTUbpvjfo37qCSUN0G6VN5t3L3vfuFctWX1eJEpXIxGQoluKwnPIyj3JB7+ZJJ6k1xFFFMjWhosNlRJvqU/XDjxVTdj4E6xXG1/X+mZiy6Ywd5HGHCAFFBIIKVADKT5jII65ktwAauttz2+1HYYZU0q3XZyQ0w6oFwRnUjwyr1OULBx6D1qu+pE8CN5kWrfMIZWQzJtr7byPJaQUKHzykUp45h0DqOeZgs42ceuXp5EpgwutlFTFE43AuB0v+9lZZXDz79rFHisO6RbfbKiEP268oSVjPT+UQgpyPTNdoxIbkoCm1BQ/qodjtPqQXG0OFB5klSQeU+o9K44CumMcGHVt/O/6ELR06h4WllUm3N2qUvKnYrbwdCTk91gDmJGDn31VrxW6qRq/iA1hMZXzx2JQgtkHI/YoS0rHuKkKPzqfXEvv9b9kdEyFMyGndUzm1N22FkFQUenjLT5IT395AHmSKr33nJLzjzq1OuuKK1rWclRJyST5muk8J0T2l9Y8WBFh15n7D3SRxDVNdlp273uengFa/wJ786N1jtJpvRMSc3B1TZongP2p88i3gkkl1rPRYIOTjqkk5GMEyjqgK3XGXaJ8edBkvQ5sdwOsyI6yhxtYOQpKh1BB8xVv/BXv3M362hRMvK0r1HaHzb7g4lIT45CQpD2B0HMk4OPxJVgAYFMtbS9neVuxSvFJm+Ep/qKKKyVYRWQUcVjW4iIpSEnHcZoQvlOR4cpwepz+dNLxLb3xdgNp7nqdxCJFxUREtsVZwHpSweQH91IClq9yCB1Ip5Ls19xwfwmq0vpUtVPP6x0PpoLIjxYD1xUgH7ynXPDBI9wZOP4jVqmiEsoadlHI7K0lQn1FqG5atvs+83eY5Puk95UiTJeOVOLUckn+7sOwpOoopu20CzkUUUUIRT9cEv+fSL/AMBJ/wDEUwtPvwqwpFvvN11AwpTL0dCYzLo8irJX+gT+dY2MkCgmB5i310TDw/RSYhicMEW5N9fAC5+ysTbcW0rmQopV6g19zcpJGPGP6UwSNxtRN9riT/E0g/8ArWw3ujqBHeQ0v+JlP9lcR7jKNiF3t3DdXyc0+p/Cg3viJid4dZonSXpTyLtJSHZCytZR4quTqf3cVw9PBxN2gN65TfOUh28BTz2Pu+KnAPKPLoUn50z9d3w+QS0kbx4D20PuvnrF6KXDq+Wlm+Zp++o9iirHPoqowRo3X0jmyXJ8Zvlz25W1nOP6X6VXHTxcNfE1qHht1NKm2xhq6We4BCbhan1FCXgnPKpCwCULHMrBwRhRyD0xNVRuliLG7rMjcGuuVdHRTQbGcVGgd/YyG7DcvZL4Ec71kn4blIx3KRnDiR/OQTjpnHanfpUcxzDlcLFaAIOoQBk4HeuhbR4baU/zQBSPAa8WSn0T9o0tV4X6vm+0H2lIPmO9VffSp6beibhaJvikEMyrY7Bz6KZdKyPyfFWi1F/6QnZZ3dbYWfPtzBevGnHPrZlCBlTjaUkPIH/LJVgdSW0irlI8RzNJUcgu0qneiit2x2h+/wB4h22MMvynUtJ9Bk4yfcO/ypqc4NBcdgqLGOleGMFydAOpXd7TbRO69cXOnLci2ZpXLzo6LeV5pTnsB5n5Dzw9f+IzRXs5a+pzn/We0u82fXPN/wDK66w2WNp2zQ7bETyR4zYbT6nHcn3k5J95rfrmNZitRUSl0by1vIA2X1Rg3COHYdSNjqIWySEfEXAO15gX2A2FvMpnJnDNZHXiqNdJzDZ/AsIXj54Fd7pvTMXbzT7NutjfjN+IVOLkPBCnFnurOMZ6AY9BXTV83WG5DZQ62l1B7pWkEH5VTmrqmoYGSvJC26PAcNw6Uz0cIY8i1xf9b29FpfWkhH8rbZI97ZQsforNH15HR/KtyWf95HWB+eMUfUUds5jLehn/AGDhCf8ApOR+lbzKFNtJStwuqAwVkAE/l0qlotoB/M/z2XE7kaJj7oacSzEkoblxnCuO8oHlCsdUq8wCMfkPhUadUaAv2j3FC5251pkHAkIHO0r0wsdPketTPrFxtDzakOJStChhSVDII9CK26DF5aJvZ2zN8PwUi4/wdSY6/vBcWS2tcag22uPwQoI0VLK/bH6Svz/jGAq3ufiMFXhpV/RwUj5AVxGreGtlEJb2nZzy5CBn2WaUkL9yVADB+I+Ypthx2klIDiWk+P5XIazgHGKUOfGGyAf4nU+ht9PpdMhZ7xO09dYlztkt6BcIjqXo8mOsocaWk5CkkdiDVyvCdvoN/tnrffZXIi+xFmBdW2xge0IAPOB5BaVJX6AqI/DVMUmO7DkOsPtqaeaUULbWMFKgcEEetWFfRNQbu6dwnVx3f8HlmGBIUD4Zkp8XKE+p5FpKsdhyZ7ir1exr4c/MLn8V2vylWHW1jwmeYj7S+vyrcoopaV1FeKSFAggEHoQfOvaKEKpjjq4Mpezt/l610jBW/oOe6XHmGEZ+qXVHqggdmST9hXZOeQ/hKma4btPC4asl3VxOW7ezhBPk45kD/tC/zq8WdBjXOE/DmR2pcSQ2pp5h9AWhxChhSVJPQggkEGoga24JYO3qLpc9uIzi4Ut8yn7MpRWtnoBhgnqpIwSEElQycE9ALNVWyOo3xAXcdPTn7Ji4XjpWYzBJVPDWNN9drgfDry1sdUxtFZvsORXlsvNradbUUrbcSUqSR3BB7GsKQ19Wg31CK8oJwK9oX6iiiihCKKKKEIrSjSHF3OYypWUIS2pAx2yDn9RW7Tn7S8N193DvTN2mhyz6dU1yOSHUYdfAOU+CkjqOqvtHp6ZxipY43SHK0arNr6+nw6HvFS/K0e/QDmegUdRwlX/frd+3tWBn2K0ykhd4uqkZahhJAKsdOZaxgJQOpIJOAFEWkbXbY2DZ7Q9s0ppqJ7Ja4KOVPMcuOrPVTjivxLUepPyGAAAp6T0jatEWRi1WeKmJEa64HVS1ealHupR9T/VSxTg2SXsWRPN8o/n4Xyti1RBW4hNV07MrXm9vufU6+qKKKK8rKRRRRQhFFFFCFwu4my+mNykFy5QyxcAMJuETCHh6AnGFD3KB92KjbrThR1Xp9bjtnUzqCGOqQ0Q2+B70KOD/AESfhUzKKqS0scupGqa8K4nxLCQGQvzMH9rtR6cx6FVh6psVytVyiWu5W+Vb3VLLrjcplTauVGD2IHdRTWZIFWYzYEa5R1R5kZqUwr7zT6AtJ+IPSot8Tej7DaYEhUGyW6GpAC0mPEbbKVeowO9ZE9L2Wzl1/BOK3YoDnhyn/b9v1Uc6K2GkpIi5APMkZ6d6draOxW25XppEu3RJSDjKX2ErHf0Iqi1tzZPM9T2MfaWumhjxnpbyWWGlvOrOEttpKlE+4CnK0hw5a31aULNs+p4iv9PcyWunuRgrP5Y99TYsunbVYGeS12yHbUqA5kxI6GgfjygUpVsRUDSMzjdchxXjyqikdBTQhpHMnN9BYe90z23XDJpjRa2ZlxB1Bc0faDkpADKFeqW+o+aifUYp4O1e0VqMjbGLMFlyqtxCqxGTtquQvd15eQ2HoiiiipFnoooooQv/2Q==
testBase64FileString;


    /**
     * 表单文件上传
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     * @author lyl
     */
    public function formDataFile(Request $request): array
    {
        $file = $request->file('file');

        $filePath = StorageService::ossService()->fileUploadByBinary($file, $this->testFolderPath);
        // 访问链接
        $visitUrl = StorageService::ossService()->getVisitUrl($filePath);

        return [
            'file_path' => $filePath,
            'visit_url' => $visitUrl,
        ];
    }

    /**
     * Base64字符串文件上传
     *
     * @return array
     * @throws \Exception
     * @author lyl
     */
    public function base64(): array
    {
        $base64FileString = $this->testBase64FileString;

        $filePath = StorageService::ossService()->fileUploadByBase64($base64FileString, $this->testFolderPath);
        // 访问链接
        $visitUrl = StorageService::ossService()->getVisitUrl($filePath);

        return [
            'file_path' => $filePath,
            'visit_url' => $visitUrl,
        ];
    }

    /**
     * 生成并上传二维码
     *
     * @return array
     * @throws \Exception
     * @author lyl
     */
    public function qrCode(): array
    {
        $member_id = 1;
        $link      = 'http://www.baidu.com';

        // 生成二维码文件内容
        $qrCode = QrCode::format('png')
            ->size(200)
            ->margin(1)
            ->encoding('UTF-8')
            ->errorCorrection('L')
            ->generate($link);

        $filePath = StorageService::ossService()->fileUploadByContents($qrCode, $this->testFolderPath . '/qr_code', 'png');

        // 访问链接
        $visitUrl = StorageService::ossService()->getVisitUrl($filePath);

        return [
            'file_path' => $filePath,
            'visit_url' => $visitUrl,
        ];
    }

}